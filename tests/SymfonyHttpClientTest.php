<?php

declare(strict_types=1);

namespace Grifix\HttpClient\Tests;

use Grifix\HttpClient\Response;
use Grifix\HttpClient\SymfonyHttpClient;
use Mockery\Mock;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class SymfonyHttpClientTest extends TestCase
{
    /**
     * @dataProvider itWorksDataProvider
     */
    public function testItWorks(string $method): void
    {
        $httpClient = new SymfonyHttpClient(
            $this->createSymfonyClientMock(
                $method,
                'http://localhost',
                [
                    'id' => '1'
                ],
                [
                    'content-type' => 'text'
                ],
                'test'
            )
        );
        if (in_array($method, ['POST', 'PUT', 'PATCH'])) {
            self::assertEquals(
                $this->createResponse(),
                $httpClient->$method(
                    'http://localhost',
                    'test',
                    ['id' => '1'],
                    ['content-type' => 'text'],
                )
            );
        } else {
            self::assertEquals(
                $this->createResponse(),
                $httpClient->$method(
                    'http://localhost',
                    ['id' => '1'],
                    ['content-type' => 'text'],
                    'test'
                )
            );
        }
    }

    public function itWorksDataProvider(): array
    {
        return [
            'options' => ['OPTIONS'],
            'get' => ['GET'],
            'head' => ['HEAD'],
            'put' => ['PUT'],
            'post' => ['POST'],
            'delete' => ['DELETE'],
            'patch' => ['PATCH']
        ];
    }

    private function createSymfonyClientMock(
        string $method,
        string $url,
        array $queryString,
        array $headers,
        string $body
    ): Mock|HttpClientInterface {
        return \Mockery::mock(HttpClientInterface::class)
            ->allows('request')
            ->with(
                $method,
                $url,
                [
                    'query' => $queryString,
                    'headers' => $headers,
                    'body' => $body
                ]
            )
            ->andReturn($this->createResponseMock())
            ->getMock();
    }

    private function createResponseMock(): ResponseInterface
    {
        return \Mockery::mock(ResponseInterface::class)
            ->shouldReceive('getStatusCode')->andReturn(200)->getMock()
            ->shouldReceive('getContent')->andReturn('test')->getMock()
            ->shouldReceive('getHeaders')->andReturn([])->getMock();
    }

    private function createResponse(): Response
    {
        return new Response(
            200,
            'test',
            []
        );
    }
}
