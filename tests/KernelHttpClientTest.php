<?php

declare(strict_types=1);

namespace Grifix\HttpClient\Tests;

use Grifix\HttpClient\KernelHttpClient;
use Grifix\HttpClient\Response;
use Mockery\Mock;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\Request as KernelRequest;
use Symfony\Component\HttpFoundation\Response as KernelResponse;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\KernelInterface;

final class KernelHttpClientTest extends TestCase
{
    /**
     * @dataProvider itWorksDataProvider
     */
    public function testItWorks(
        string $method,
    ): void {
        $httpClient = new KernelHttpClient(
            $this->createKernelMock(
                $this->createRequest(
                    'http://localhost',
                    $method,
                    'test',
                    ['id' => 1],
                    ['content-type' => 'text']
                )
            )
        );

        if (in_array($method, ['post', 'put', 'patch'])) {
            self::assertEquals(
                $this->createResponse(),
                $httpClient->$method(
                    'http://localhost',
                    'test',
                    ['id' => '1'],
                    ['content-type' => 'text'],
                )
            );
        } else {
            self::assertEquals(
                $this->createResponse(),
                $httpClient->$method(
                    'http://localhost',
                    ['id' => '1'],
                    ['content-type' => 'text'],
                    'test'
                )
            );
        }
    }

    public function itWorksDataProvider(): array
    {
        return [
            'get' => ['get']
        ];
    }

    private function createKernelMock(KernelRequest $expectedRequest): KernelInterface|Mock
    {
        return \Mockery::mock(KernelInterface::class)
            ->shouldReceive('handle')->andReturnUsing(function (KernelRequest $request) use ($expectedRequest) {
                self::assertEquals($expectedRequest->getMethod(), $request->getMethod());
                self::assertEquals($expectedRequest->headers->all(), $request->headers->all());
                self::assertEquals($expectedRequest->query->all(), $request->query->all());
                self::assertEquals($expectedRequest->getContent(), $request->getContent());
                self::assertEquals($expectedRequest->getUri(), $request->getUri());
                return $this->createResponseMock();
            })
            ->getMock();
    }

    private function createResponseMock(): KernelResponse|Mock
    {
        return \Mockery::mock(KernelResponse::class)
            ->shouldReceive('getStatusCode')->andReturn(200)->getMock()
            ->shouldReceive('getContent')->andReturn('test')
            ->set(
                'headers',
                \Mockery::mock(ResponseHeaderBag::class)->shouldReceive('all')->andReturn([])->getMock()
            )->getMock();
    }

    private function createRequest(
        string $uri,
        string $method,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): KernelRequest {
        $result = KernelRequest::create($uri, $method, $queryString, content: $body);
        $result->headers->add($headers);
        return $result;
    }

    private function createResponse(): Response
    {
        return new Response(
            200,
            'test',
            []
        );
    }
}
