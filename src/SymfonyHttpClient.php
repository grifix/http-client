<?php

declare(strict_types=1);

namespace Grifix\HttpClient;

use Symfony\Contracts\HttpClient\HttpClientInterface as SymfonyHttpClientInterfaceAlias;

final class SymfonyHttpClient implements HttpClientInterface
{
    public function __construct(private readonly SymfonyHttpClientInterfaceAlias $httpClient)
    {
    }

    public function post(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): ResponseInterface {
        return $this->sendRequest(
            'POST',
            $url,
            $body,
            $queryString,
            $headers
        );
    }

    public function put(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): ResponseInterface {
        return $this->sendRequest(
            'PUT',
            $url,
            $body,
            $queryString,
            $headers
        );
    }

    public function delete(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface {
        return $this->sendRequest(
            'DELETE',
            $url,
            $body,
            $queryString,
            $headers
        );
    }

    public function patch(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): ResponseInterface {
        return $this->sendRequest(
            'PATCH',
            $url,
            $body,
            $queryString,
            $headers
        );
    }

    public function get(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface {
        return $this->sendRequest(
            'GET',
            $url,
            $body,
            $queryString,
            $headers
        );
    }

    public function options(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface {
        return $this->sendRequest(
            'OPTIONS',
            $url,
            $body,
            $queryString,
            $headers
        );
    }

    public function head(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface {
        return $this->sendRequest(
            'HEAD',
            $url,
            $body,
            $queryString,
            $headers
        );
    }

    private function sendRequest(
        string $method,
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): ResponseInterface {
        $options = [];
        if ($queryString) {
            $options['query'] = $queryString;
        }
        if ($headers) {
            $options['headers'] = $headers;
        }

        if (null !== $body) {
            $options['body'] = $body;
        }
        $response = $this->httpClient->request($method, $url, $options);

        return new Response(
            $response->getStatusCode(),
            $response->getContent(),
            $response->getHeaders()
        );
    }
}
