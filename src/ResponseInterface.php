<?php

declare(strict_types=1);

namespace Grifix\HttpClient;

interface ResponseInterface
{
    public function getStatusCode(): int;

    public function getContent(): string;

    public function getHeaders(): array;
}
