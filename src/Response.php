<?php

declare(strict_types=1);

namespace Grifix\HttpClient;

final class Response implements ResponseInterface
{

    public function __construct(
        private readonly int $statusCode,
        private readonly string $content,
        private readonly array $headers
    ) {
    }

    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getHeaders(): array
    {
        return $this->headers;
    }
}
