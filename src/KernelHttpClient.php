<?php

declare(strict_types=1);

namespace Grifix\HttpClient;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;

final class KernelHttpClient implements HttpClientInterface
{
    private ?Response $lastResponse = null;

    public function __construct(private readonly KernelInterface $kernel)
    {
    }

    public function get(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface {
        return $this->sendRequest('get', $url, $body, $queryString, $headers);
    }

    public function head(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface {
        return $this->sendRequest('head', $url, $body, $queryString, $headers);
    }

    public function delete(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface {
        return $this->sendRequest('delete', $url, $body, $queryString, $headers);
    }

    public function options(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface {
        return $this->sendRequest('options', $url, $body, $queryString, $headers);
    }

    public function post(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): ResponseInterface {
        return $this->sendRequest('post', $url, $body, $queryString, $headers);
    }

    public function put(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): ResponseInterface {
        return $this->sendRequest('put', $url, $body, $queryString, $headers);
    }

    public function patch(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): ResponseInterface {
        return $this->sendRequest('patch', $url, $body, $queryString, $headers);
    }

    private function sendRequest(
        string $method,
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): ResponseInterface {
        $request = Request::create($url, $method, $queryString, content: $body);
        if ($headers) {
            $request->headers->add($headers);
        }
        $lastResponse = $this->kernel->handle($request);
        $this->lastResponse = new Response(
            $lastResponse->getStatusCode(),
            $lastResponse->getContent(),
            $lastResponse->headers->all()
        );

        return $this->lastResponse;
    }

    public function getLastResponse(): Response
    {
        return $this->lastResponse;
    }
}
