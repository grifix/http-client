<?php

declare(strict_types=1);

namespace Grifix\HttpClient;

interface HttpClientInterface
{

    public function get(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface;

    public function head(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface;

    public function delete(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface;

    public function options(
        string $url,
        array $queryString = [],
        array $headers = [],
        ?string $body = null,
    ): ResponseInterface;

    public function post(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): ResponseInterface;

    public function put(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): ResponseInterface;

    public function patch(
        string $url,
        ?string $body = null,
        array $queryString = [],
        array $headers = []
    ): ResponseInterface;
}
